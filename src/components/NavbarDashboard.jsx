/** @format */

import React from "react";
import "../assets/css/NavbarDashboard.css";
import Rectangle6 from "../assets/icon/Rectangle 6.svg";
import Rectangle63 from "../assets/icon/Rectangle 63.svg";
import HomeIcon from "../assets/icon/Home_Icon.svg";
import TruckIcon from "../assets/icon/Truck_Icon.svg";
import Group15 from "../assets/icon/Group 15.svg";
import FiMenu from "../assets/icon/fi_menu.svg";
import { Dropdown } from "react-bootstrap";

function NavbarDashboard() {
  const handleLogout = async () => {
    localStorage.clear();
    window.location.href = "/";
  };
  return (
    <>
      <section class='navbar-section'>
        <nav class='navbar navbar-expand-lg border-bottom ms-5'>
          <div class='container-fluid'>
            <div class='sidebar-toggler ps-5'>
              <a href='#'>
                <img class='ms-5 me-5 collapseSidebar' src={Rectangle6} />
              </a>
              <button class='btn ms-5' id='toggleSidebar'>
                <img src={FiMenu} />
              </button>
            </div>
            <button
              class='navbar-toggler'
              type='button'
              data-bs-toggle='collapse'
              data-bs-target='#navbarSupportedContent'
              aria-controls='navbarSupportedContent'
              aria-expanded='false'
              aria-label='Toggle navigation'>
              <img src={FiMenu} alt='' />
            </button>
            <div class='collapse navbar-collapse' id='navbarSupportedContent'>
              <ul class='navbar-nav ms-auto mt-2 mt-lg-3'>
                <li class='nav-item'>
                  <form class='d-flex ms-5'>
                    <input
                      class='form-control'
                      type='search'
                      placeholder='Search'
                      aria-label='Search'
                    />
                    <button
                      class='btn btn-outline-primary border-3 me-3 fw-bold'
                      type='submit'>
                      Search
                    </button>
                  </form>
                </li>
                <li class='nav-item'>
                  <div class='dropdown'>
                      {/* <img
                        src='images/icon/Group 15.svg'
                        width='32'
                        height='32'
                        class='rounded-circle me-2'
                        style='object-fit:cover;'
                      /> */}

                      <Dropdown >
                        <Dropdown.Toggle className="d-flex flex-row justify-content-center align-items-center link-dark text-decoration-none  me-2" variant='custom' id='dropdown-basic' >
                        <p class='my-2 me-2'>Unis Badri</p>
                        <img
                        src={Group15}
                        width='32'
                        height='32'
                        class='rounded-circle me-2'
                      />
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                          <Dropdown.Item onClick={handleLogout}>
                            Logout
                          </Dropdown.Item>
                          </Dropdown.Menu>
                      </Dropdown>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </section>
      <section class='sidebar-section'>
        {/* Sidebar - Main Menu */}
        <div class='main-menu'>
          <a href='/dashboard'>
            <div class='box text-center d-flex justify-content-center align-items-center'>
              <img class='side-icon' src={Rectangle63} />
            </div>
          </a>
          <a href='/dashboard'>
            <div class='box text-center py-2'>
              <img class='side-icon' src={HomeIcon} />
              <div>Dashboard</div>
            </div>
          </a>
          <a href='/cars'>
            <div class='box text-center py-2'>
              <img class='side-icon' src={TruckIcon} />
              <div>Cars</div>
            </div>
          </a>
        </div>
        <div class='collapseSidebar'>
          {/* Sidebar - Menu List */}
          <div class='menu-list pt-3'>
            <h4 class='text-secondary px-3 py-2'>CARS</h4>
            <a href='/dashboard'>
              <div class='list-menu'>
                <p class='m-0'>Dashboard</p>
              </div>
            </a>
          </div>
        </div>
      </section>
    </>
  );
}

export default NavbarDashboard;
