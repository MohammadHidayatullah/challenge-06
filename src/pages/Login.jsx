/** @format */

import React, { useEffect } from "react";
import "../assets/css/NavbarDashboard.css";
import { Form } from "react-bootstrap";
import Image2 from "../assets/img/image 2.svg";
import Rectangle6 from "../assets/icon/Rectangle 62.svg";
import { useNavigate } from "react-router";
import axios from "axios";
import { Link } from "react-router-dom";

function Login() {
  const [loginData, setLoginData] = React.useState({
    // email: "",
    // password: "",
    email: "dayat@gmail.com",
    // email: "admin@mail.com",
    password: "123456",
  });

  const navigate = useNavigate();

  useEffect(() => {
    if (localStorage.getItem("access_token")) {
      navigate("/");
    }
  }, []);

  const handleLogin = async () => {
    try {
      const res = await axios({
        method: "POST",
        url: "https://rent-car-appx.herokuapp.com/admin/auth/login",
        data: loginData,
      });
      // console.log(res);
      if (res.data.role === "admin") {
        localStorage.setItem("access_token", res.data.access_token);
        navigate("/dashboard", { replace: true });
      } else if (res.data.role === "Customer") {
        localStorage.setItem("access_token", res.data.access_token);
        navigate("/home", { replace: true });
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <section class='vh-100'>
        <div class='container-fluid'>
          <div class='row login'>
            <div class='col-8 left-side'>
              <img src={Image2} class='banner-login w-100 vh-100' alt='' />
              {/* <img
                src='images/image 2.svg'
                class='w-100 vh-100'
                style='object-fit: cover;'
                alt=''
              /> */}
            </div>
            <div class='content col-4'>
              <div class='form-head'>
                <img src={Rectangle6} alt='' />
                <h1>Welcome back</h1>
              </div>

              <div class='form-content'>
                <Form.Group action=''>
                  <div class='mb-3'>
                    <Form.Label for='inputEmail' class='form-label'>
                      Email
                    </Form.Label>
                    <Form.Control
                      type='email'
                      class='form-control'
                      id='inputEmail'
                      placeholder='Contoh: johndee@gmail.com'
                      value={loginData.email}
                      onChange={(e) =>
                        setLoginData({
                          ...loginData,
                          email: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div class='mb-3'>
                    <Form.Label for='inputPassword' class='form-label'>
                      Password
                    </Form.Label>
                    <Form.Control
                      type='password'
                      class='form-control'
                      id='inputPassword'
                      placeholder='6+ karakter'
                      value={loginData.password}
                      onChange={(e) =>
                        setLoginData({
                          ...loginData,
                          password: e.target.value,
                        })
                      }
                    />
                  </div>
                  <a
                    class='btn btn-primary'
                    role='button'
                    onClick={handleLogin}>
                    Sign In
                  </a>
                  <p>
                    Don't have an account?{" "}
                    <Link to={"/register"}>Register</Link>
                  </p>
                </Form.Group>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Login;
